import os
import pandas as pd

datapath = 'C:\\Users\\spbro\\Documents\\python\\InstaGraph\\data\\relationship_data'
root = 'spbroma'


def get_followings(user):
    path = os.path.join(datapath, user)
    path = os.path.join(path, 'following')

    if os.path.exists(path):

        files = [f.path for f in os.scandir(path) if f.is_file()]

        file = files[-1]

        followings = pd.read_json(file)
        followings = list(followings[0])
    else:
        followings = []
        print(f'No data about {user}\'s followings')

    return followings


def remove_root_followings(df: pd.DataFrame, root_followings: list):
    for f in root_followings:
        df = df[df['username'] != f]

    return df


if __name__ == '__main__':

    summary_dict = {}
    root_followings = get_followings(root)

    for f in root_followings:
        child_followings = get_followings(f)
        for user in child_followings:
            if user in summary_dict.keys():
                summary_dict[user]['followers'].append(f)
                summary_dict[user]['count'] += 1
            else:
                tmp_dict = {'followers': [f], 'count': 1}
                summary_dict[user] = tmp_dict

    df = pd.DataFrame.from_dict(summary_dict, orient='index')
    df = df.sort_values(by=['count'], ascending=False)
    df = df.reset_index()
    df = df.rename(columns={'index': 'username'})


    df_filt = remove_root_followings(df, root_followings+[root])

    with pd.option_context('display.max_rows', None, 'display.max_columns', None):  # more options can be specified also
        print(df_filt[df_filt['count'] > 3][['username', 'count']])

        # df_filt[df_filt['username']=='musiatotibadze']['followers'].iloc[0]