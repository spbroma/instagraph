# imports
from instapy import InstaPy
from instapy import smart_run
import pandas as pd

datapath = 'C:\\Users\\spbro\\Documents\\python\\InstaGraph\\data\\relationship_data'

# login credentials
auth = pd.read_json('auth.txt')
insta_username = auth['username'][0]
insta_password = auth['password'][0]


# get an InstaPy session!
# set headless_browser=True to run InstaPy in the background
session = InstaPy(username=insta_username,
                  password=insta_password,
                  headless_browser=False)


with smart_run(session):
        """ Activity flow """

        # followers = session.grab_followers(username="spbroma", amount="full", live_match=True, store_locally=True)
        following = session.grab_following(username="spbroma", amount="full", live_match=True, store_locally=True)

        # print(followers)
