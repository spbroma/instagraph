# imports
from instapy import InstaPy
from instapy import smart_run
import pandas as pd
import os
import time

datapath = 'C:\\Users\\spbro\\Documents\\python\\InstaGraph\\data\\relationship_data'
datapath = '/home/spbroma/InstaPy/logs/nashi_panelki/relationship_data/'

# login credentials

auth = pd.read_json('auth.txt')
insta_username = auth['username'][0]
insta_password = auth['password'][0]

# get an InstaPy session!
# set headless_browser=True to run InstaPy in the background
session = InstaPy(username=insta_username,
                  password=insta_password,
                  headless_browser=False)

followings = list(pd.read_json('followings.json')[0])

with smart_run(session):
        """ Activity flow """

        # followers = session.grab_followers(username="spbroma", amount="full", live_match=True, store_locally=True)
        for i, username in enumerate(followings):
            path = os.path.join(datapath, username)
            path = os.path.join(path, 'following')

            if not os.path.exists(path):

                print('==========', i, '/', len(followings), ':', username,'==========')
                following = session.grab_following(username=username, amount="full", live_match=True, store_locally=True)
                time.sleep(60)
            else:
                print(f'========== {username} already exist ==========')


        # print(followers)
